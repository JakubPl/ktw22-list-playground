package pl.sda;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) {
        Node<String> kotaNode = new Node<>("kota", null);
        Node<String> rudegoNode = new Node<>("rudego", kotaNode);
        Node<String> maNode = new Node<>("ma", rudegoNode);
        Node<String> alaNode = new Node<>("Ala", maNode);


        Node<String> head = alaNode;

        //printNodes(head);

        //printInDescOrder(head);


        // new ArrayList<String>(1000); <- arraylista z poczatkowo zarezerwona pamiecia na 1000 elementow

        List<Integer> list = IntStream.range(0, 100_000)
                .boxed()
                .collect(Collectors.toCollection(() -> new LinkedList<>()));

        /*for (String s : list) {
            System.out.println(s);
        }*/

        Iterator<Integer> iterator = list.iterator();
        long startTimeIterator = System.currentTimeMillis();
        while(iterator.hasNext()) {
            Integer next = iterator.next();
        }
        long endTimeIterator = System.currentTimeMillis();
        System.out.println(endTimeIterator - startTimeIterator);

        long startTimeForGetI = System.currentTimeMillis();
        for(int i = 0; i < list.size(); i ++) {
            list.get(i);
        }
        long endTimeForGetI = System.currentTimeMillis();
        System.out.println(endTimeForGetI - startTimeForGetI);





        //TODO: iterator w javie
        //TODO: jak to zrobic w javie nieniskopoziomowo
    }

    private static void printInDescOrder(Node<String> head) {
        System.out.println("Elementy od najdluzszego");

        while (head != null) {
            Node<String> iterator = head;
            Node<String> maxNode = head;
            Node<String> predecessorOfIterator = null;
            Node<String> predecessorOfMaxNode = null;
            while (iterator != null) {
                if (maxNode.getValue().length() < iterator.getValue().length()) {
                    predecessorOfMaxNode = predecessorOfIterator;
                    maxNode = iterator;
                }
                predecessorOfIterator = iterator;
                iterator = iterator.getNext();
            }
            System.out.println(maxNode.getValue());
            if (maxNode == head) {
                head = maxNode.getNext();
            } else {
                predecessorOfMaxNode.setNext(maxNode.getNext());
            }
            //usunac maxNode
            //}
        }
    }

    private static void printNodes(Node head) {
        Node iterator = head;
        while (iterator != null) {
            System.out.println(iterator.getValue());
            iterator = iterator.getNext();
        }
    }
}
